#-------------------------------------------------------------------------------
# Name:        constantes - Proutpulsor
#
# Author:      Sivigik
#
# Licence:     <GNU GENERAL PUBLIC LICENSE>
#-------------------------------------------------------------------------------

import pygame
from pygame.locals import *

ASTRONAUT = pygame.image.load("images/astronaut.png").convert_alpha()
ASTRONAUTEXT = pygame.image.load("images/astronautext.png").convert_alpha()

BACKGROUND = pygame.image.load("images/background.png").convert_alpha()
